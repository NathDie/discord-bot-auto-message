const Discord = require('discord.js');
const bot = new Discord.Client();

bot.on('ready', function() {
    console.log("I'm ready");
})

setInterval(function() {
    var date = new Date();
    var day = date.getDay();
    var hour = date.getHours();
    var minute = date.getMinutes();
    var month = date.getMonth();
    //var seconde = date.getSeconds();

    // May (month)
    if (month === 5) {
        // Friday at 9.58 a.m.
        if (day === 5) {
            if (hour === 09) {
                if (minute === 58) {
                    bot.channels.cache.get("CHANNEL_ID").send("YOUR_MESSAGE");
                }
            }
        
        } 
        // Monday at 9.58 a.m.
        else if (day === 1) {
            if (hour === 09) {
                if (minute === 58) {
                    bot.channels.cache.get("CHANNEL_ID").send("YOUR_MESSAGE");
                }
            }
        }
    }
}, 60000);


bot.login("TOKEN_ID");
